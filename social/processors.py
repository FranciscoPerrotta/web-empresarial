# Archivo en el cual definimos una clave para poder acceder a ella desde cualquier lugar de nuestra aplicacion. 
# Configurado en settings TEMPLATES  OPTIONS

from .models import Link

def ctx_dict(request):
    ctx = {}
    links = Link.objects.all()
    for link in links:
        ctx[link.key] = link.url
    return ctx