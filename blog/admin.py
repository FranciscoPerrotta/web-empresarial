from django.contrib import admin
from .models import Category, Post

# Register your models here.
class CategoryAdmin(admin.ModelAdmin):
    readonly_fields = ('created','updated')

class PostAdmin(admin.ModelAdmin):
    readonly_fields = ('created','updated')
    list_display = ('title', 'author', 'published', 'post_categories')
    ordering = ('author', 'published') # Para ordenar
    search_fields = ('title', 'content', 'author__username', 'categories__name') # __ para que django entienda lo que queremos buscar del modelo relacionado
    date_hierarchy = 'published'
    list_filter = ('author__username', 'categories__name')

    def post_categories(self, obj):
        return ", ".join([c.name for c in obj.categories.all().order_by("name")]) # Lista las categorias separadas por coma para poder mostrarlas
    post_categories.short_description = "Categorias" # Para cambiar el nombre para que aparexca bien en la lista


admin.site.register(Category, CategoryAdmin)
admin.site.register(Post, PostAdmin)